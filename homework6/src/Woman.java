import java.util.Arrays;

public final class Woman extends Human{
    public void makeup(){
        System.out.println("I'm painting my lips now");
    }

    public Woman(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman() {
    }

    @Override
    public void greetPet() {
        System.out.println("Good boy, " + getFamily().getPet().getNickname());
    }
    @Override
    public String toString() {
        return "Woman{name='" + getName() + "', surname='" + getSurname() + "', year=" + getYear() + ", iq=" + getIq() + ", schedule=" + Arrays.deepToString(getSchedule()) + "}";
    }
}
