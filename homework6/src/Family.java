import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Family implements HumanCreator {
    private Man father;
    private Woman mother;
    private Random random;
    private Human child;
    private String[] maleNames = new String[] {"John", "Michael", "William", "David", "James"};
    private String[] femaleNames = new String[] {"Emily", "Emma", "Olivia", "Sophia", "Ava"};;
    private Human[] children;
    private Pet pet;

    static {
        System.out.println("Class Family is loading...");
    }

    {
        random = new Random();
        System.out.println("The new object Family is creating\n");
    }
    public Family(Man father, Woman mother) {
        this.father = father;
        this.mother = mother;
        this.children = new Human[0];
    }
    @Override
    public Human bornChild() {
        boolean isMale = random.nextBoolean();
        String firstName = getRandomName(isMale);
        int iq = (father.getIq() + mother.getIq()) / 2;
        if (isMale) {
            child =  new Man(firstName, father.getSurname(), 0, iq);
        } else {
             child = new Woman(firstName,father.getSurname(), 0, iq);
        }
        addChild(child);
        return child;
    }

    private String getRandomName(boolean isMale) {
        if (isMale) {
            int index = random.nextInt(maleNames.length);
            return maleNames[index];
        } else {
            int index = random.nextInt(femaleNames.length);
            return femaleNames[index];
        }
    }

    public void addChild(Human child) {
        child.setFamily(this);
        Human[] updatedChildren = Arrays.copyOf(children, children.length + 1);
        updatedChildren[children.length] = child;
        children = updatedChildren;
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index >= children.length) {
            return false;
        }

        Human child = children[index];
        child.setFamily(null);

        Human[] updatedChildren = new Human[children.length - 1];
        int current = 0;

        for (int i = 0; i < children.length; i++) {
            if (i != index) {
                updatedChildren[current] = children[i];
                current++;
            }
        }

        children = updatedChildren;
        return true;
    }

    public boolean deleteChild(Human child) {
        int index = -1;
        for (int i = 0; i < children.length; i++) {
            if (child.equals(children[i])) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            return false;
        }
        return deleteChild(index);
    }

    public int countFamily() {
        return children.length + 2;
    }

    public Human getChild() {
        return child;
    }

    public void setChild(Human child) {
        this.child = child;
    }

    public Woman getMother() {
        return mother;
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }

    public Man getFather() {
        return father;
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Arrays.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, Arrays.hashCode(children), pet);
    }

    @Override
    protected void finalize() {
        System.out.println("Family is deleting!");
    }
}
