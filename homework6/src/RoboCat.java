import java.util.Arrays;

public class RoboCat extends Pet{
    private final Species species = Species.ROBOCAT;

    @Override
    public Species getSpecies() {
        return species;
    }

    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    public RoboCat() {
    }

    @Override
    public void respond() {
        System.out.println("Hello owner, " + getNickname() + " missed you!");
    }
    @Override
    public String toString() {
        return this.species + "{nickname='" + getNickname() + "', age=" + getAge() +
                ", trickLevel=" + getTrickLevel() + ", habits=" + Arrays.toString(getHabits()) +
                ", canFly=" + getSpecies().getCanFly() + ", hasFur=" + getSpecies().getHasFur() +
                ", numberOfLegs=" + getSpecies().getNumberOfLegs() + "}";
    }
}
