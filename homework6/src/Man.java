import javax.sound.midi.Soundbank;
import java.util.Arrays;

public final class Man extends Human{
    public void repairCar(){
        System.out.println("Give me the key");
    }

    public Man(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man() {
    }


    @Override
    public void greetPet() {
        System.out.println("Good boy, " + getFamily().getPet().getNickname());
    }
    @Override
    public String toString() {
        return "Man{name='" + getName() + "', surname='" + getSurname() + "', year=" + getYear() + ", iq=" + getIq() + ", schedule=" + Arrays.deepToString(getSchedule()) + "}";
    }
}
