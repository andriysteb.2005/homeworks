package Structure.model;

import java.util.Map;
import java.util.Set;

public final class Woman extends Human{
    public void makeup(){
        System.out.println("I'm painting my lips now");
    }

    public Woman(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    public Woman(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman() {
    }

    @Override
    public void greetPet() {
        if (getFamily() != null) {
            Set<Pet> pets = getFamily().getPets();
            for (Pet pet : pets) {
                System.out.println("Good boy " + pet.getNickname());
            }
        }
    }
    @Override
    public String toString() {
        return "Woman{name='" + getName() + "', surname='" + getSurname() + "', year=" + getYear() + ", iq=" + getIq() + ", schedule=" + getSchedule() + "}";
    }
}
