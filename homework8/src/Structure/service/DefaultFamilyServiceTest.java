package Structure.service;

import Structure.dao.FamilyDao;
import Structure.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class DefaultFamilyServiceTest {
    private FamilyDao familyDao;
    private DefaultFamilyService familyService;

    @BeforeEach
    public void setup() {
        familyDao = mock(FamilyDao.class);
        familyService = new DefaultFamilyService(familyDao);
    }

    @Test
    public void testGetAllFamilies() {
        List<Family> expectedFamilies = Arrays.asList(
                new Family(new Man("John", "Smith", 40, 120), new Woman("Jane", "Smith", 40, 100)),
                new Family(new Man("Mike", "Holmes", 60, 90), new Woman("Emily", "Holmes", 55, 100))
        );
        when(familyDao.getAllFamilies()).thenReturn(expectedFamilies);

        List<Family> actualFamilies = familyService.getAllFamilies();

        assertEquals(expectedFamilies, actualFamilies);
    }
    @Test
    public void testDisplayAllFamilies() {

        List<Family> expectedFamilies = Arrays.asList(
                new Family(new Man("John", "Smith", 40, 120), new Woman("Jane", "Smith", 40, 100)),
                new Family(new Man("Mike", "Holmes", 60, 90), new Woman("Emily", "Holmes", 55, 100))
        );
        when(familyDao.getAllFamilies()).thenReturn(expectedFamilies);


        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));


        familyService.displayAllFamilies();
        System.setOut(System.out);


        String expectedOutput = "Family :\n" +
                expectedFamilies.get(0) + "\n" +
                "Family :\n" +
                expectedFamilies.get(1) + "\n";
        assertEquals(expectedOutput, outputStream.toString());
    }
    @Test
    public void testCountFamiliesWithMemberNumber() {

        List<Family> expectedFamilies = Arrays.asList(
                new Family(new Man("John", "Smith", 40, 120), new Woman("Jane", "Smith", 40, 100)),
                new Family(new Man("Mike", "Holmes", 60, 90), new Woman("Emily", "Holmes", 55, 100))
        );
        when(familyDao.getAllFamilies()).thenReturn(expectedFamilies);


        int count = familyService.countFamiliesWithMemberNumber(2);


        assertEquals(2, count);
    }
    @Test
    public void testGetFamiliesBiggerThan() {

        List<Family> expectedFamilies = Arrays.asList(
                new Family(new Man("John", "Smith", 40, 120), new Woman("Jane", "Smith", 40, 100)),
                new Family(new Man("Mike", "Holmes", 60, 90), new Woman("Emily", "Holmes", 55, 100))
        );

        when(familyDao.getAllFamilies()).thenReturn(expectedFamilies);

        List<Family> result = familyService.getFamiliesBiggerThan(2);

        assertEquals(0, result.size());
    }

    @Test
    public void testGetFamiliesLessThan() {

        List<Family> expectedFamilies = Arrays.asList(
                new Family(new Man("John", "Smith", 40, 120), new Woman("Jane", "Smith", 40, 100)),
                new Family(new Man("Mike", "Holmes", 60, 90), new Woman("Emily", "Holmes", 55, 100))
        );

        when(familyDao.getAllFamilies()).thenReturn(expectedFamilies);

        List<Family> result = familyService.getFamiliesLessThan(4);

        assertEquals(2, result.size());
    }
    @Test
    public void testCreateNewFamily() {

        Human father = new Man("John", "Smith", 40, 120);
        Human mother = new Woman("Jane", "Smith", 40, 100);
        Family expectedFamily = new Family((Man) father, (Woman) mother);

        familyService.createNewFamily(father, mother);

        verify(familyDao).saveFamily(expectedFamily);
    }

    @Test
    public void testDeleteFamilyByIndex() {

        List<Family> allFamilies = new ArrayList<>();
        Family family1 = new Family(new Man("John", "Smith", 40, 120), new Woman("Jane", "Smith", 40, 100));
        Family family2 = new Family(new Man("Mike", "Holmes", 60, 90), new Woman("Emily", "Holmes", 55, 100));
        allFamilies.add(family1);
        allFamilies.add(family2);

        when(familyDao.getAllFamilies()).thenReturn(allFamilies);

        familyService.deleteFamilyByIndex(1);

        verify(familyDao).deleteFamily(family2);
    }
    @Test
    public void testBornChild() {

        Family family = new Family(new Man("John", "Smith", 40, 120), new Woman("Jane", "Smith", 40, 100));
        String maleName = "James";
        String femaleName = "Lily";

        Family result = familyService.bornChild(family, maleName, femaleName);
        Human child = family.getChildren().get(0);

        verify(familyDao).updateFamily(family);

        assertEquals(maleName, child.getName());
        assertTrue(child instanceof Man);
        assertEquals(family, result);
    }

    @Test
    public void testAdoptChild() {

        Family family = new Family(new Man("John", "Smith", 40, 120), new Woman("Jane", "Smith", 40, 100));
        Human child = new Human("Mike","Smith", 10, 200 );

        Family result = familyService.adoptChild(family, child);

        verify(familyDao).updateFamily(family);

        assertTrue(family.getChildren().contains(child));
        assertEquals(family, result);
    }
    @Test
    public void testDeleteAllChildrenOlderThen() {

        Family family = new Family(new Man("John", "Smith", 40, 120), new Woman("Jane", "Smith", 40, 100));

        Human child1 = new Human("Child1","Smith", 30, 120);
        Human child2 = new Human("Child2","Smith", 15, 120);
        Human child3 = new Human("Child3","Smith", 4, 120);


        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);

        when(familyDao.getAllFamilies()).thenReturn(Arrays.asList(family));

        familyService.deleteAllChildrenOlderThen(20);

        assertFalse(family.getChildren().contains(child1));
        assertTrue(family.getChildren().contains(child2));
        assertTrue(family.getChildren().contains(child3));

        verify(familyDao).updateFamily(family);
    }

    @Test
    public void testCount() {

        List<Family> expectedFamilies = Arrays.asList(
                new Family(new Man("John", "Smith", 40, 120), new Woman("Jane", "Smith", 40, 100)),
                new Family(new Man("Mike", "Holmes", 60, 90), new Woman("Emily", "Holmes", 55, 100))
        );
        when(familyDao.getAllFamilies()).thenReturn(expectedFamilies);

        int count = familyService.count();

        assertEquals(2, count);
    }
    @Test
    public void testGetFamilyById() {

        Family family = new Family(new Man("John", "Smith", 40, 120), new Woman("Jane", "Smith", 40, 100));

        when(familyDao.getFamilyByIndex(0)).thenReturn(family);

        Family result = familyService.getFamilyById(0);

        assertEquals(family, result);
    }
    @Test
    public void testGetPets() {

        Family family = new Family(new Man("John", "Smith", 40, 120), new Woman("Jane", "Smith", 40, 100));
        Pet pet1 = new Dog("Dog");
        Pet pet2 = new DomesticCat("Cat");

        family.addPet(pet1);
        family.addPet(pet2);

        when(familyDao.getFamilyByIndex(0)).thenReturn(family);

        List<Pet> result = familyService.getPets(0);

        assertEquals(Arrays.asList(pet1, pet2), result);
    }
    @Test
    public void testAddPet() {

        Family family = new Family(new Man("John", "Smith", 40, 120), new Woman("Jane", "Smith", 40, 100));
        Pet pet = new Dog("Dog");


        when(familyDao.getFamilyByIndex(0)).thenReturn(family);

        familyService.addPet(0, pet);

        verify(familyDao).updateFamily(family);

        assertTrue(family.getPets().contains(pet));
    }
    @Test
    public void testSaveFamily() {
        Family family = new Family(new Man("John", "Smith", 40, 120), new Woman("Jane", "Smith", 40, 100));

        familyService.saveFamily(family);

        verify(familyDao).saveFamily(family);
    }


}
