import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    private boolean timeToEat;
    Random random = new Random();

    static {
        System.out.println("Class Pet is loading...");
    }

    {
        System.out.println("The new object Pet is creating\n");
    }

    public boolean getTimeToEat() {
        int number = random.nextInt(2);
        if (number == 0) {
            return true;
        } else {
            return false;
        }
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {
    }


    public void eat() {
        System.out.println("I'm eating!");
    }

    public void respond() {
        System.out.println("Hello owner, " + nickname + " missed you!");
    }

    public void foul() {
        System.out.println("I need to cover my tracks well...");
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }
    public boolean isTimeToEat() {
        return timeToEat;
    }

    public void setTimeToEat(boolean timeToEat) {
        this.timeToEat = timeToEat;
    }

    @Override
    public String toString() {
        return species + "{nickname='" + nickname + "', age=" + age +
                ", trickLevel=" + trickLevel + ", habits=" + Arrays.toString(habits) + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pet pet = (Pet) o;
        if (pet.hashCode() != o.hashCode()) {
            return false;
        }

        if (age != pet.age || trickLevel != pet.trickLevel) {
            return false;
        }
        if (!nickname.equals(pet.nickname)) {
            return false;
        }
        return species.equals(pet.species);

    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel);
    }
}
