package Structure.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

public final class Man extends Human implements Serializable {
    public void repairCar(){
        System.out.println("Give me the key");
    }

    public Man(String name, String surname, long birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    public Man(String name, String surname, long birthDate, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public Man() {
    }

    @Override
    public void greetPet() {
        if (getFamily() != null) {
            Set<Pet> pets = getFamily().getPets();
            for (Pet pet : pets) {
                System.out.println("Good boy " + pet.getNickname());
            }
        }
    }
    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String formattedBirthDate = dateFormat.format(new Date(getBirthDate()));
        return "Man{name='" + getName() + "', surname='" + getSurname() + "', birthDate=" + formattedBirthDate + ", iq=" + getIq() + ", schedule=" + getSchedule() + "}";
    }
}
