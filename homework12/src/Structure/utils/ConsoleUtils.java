package Structure.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class ConsoleUtils {
    public static int getInputNumberValue(Scanner scanner, String error) {

        while (true) {
            try {
                return Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.out.println(error);
            }
        }
    }

    public static Date parseDate(String day, String month, String year) {
        String dateString = day + "/" + month + "/" + year;
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            System.out.println("Invalid date format. Please enter the date in the format DD/MM/YYYY.");
            return null;
        }
    }


}

