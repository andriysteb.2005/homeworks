package Structure.controller;

import Structure.model.*;
import Structure.service.DefaultFamilyService;
import Structure.utils.ConsoleUtils;

import java.util.Date;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class FamilyController {
    private static  DefaultFamilyService defaultFamilyService;


    public FamilyController(DefaultFamilyService defaultFamilyService) {
        FamilyController.defaultFamilyService = defaultFamilyService;
    }

    public static void doAll() {

        Fish fish = new Fish("bob");

        DomesticCat billPet = new DomesticCat("tom", 5, 20, Set.of("sleeping"));
        Woman billMother = new Woman("Sofia", "Gates", 610945400005L, 120);
        Man billFather = new Man("Mark", "Gates", 51690330005L, 100);
        Man Bill = new Man("Bill", "Gates", 216055L, 130,
                Map.of(DayOfWeek.MONDAY, "Go ot school", DayOfWeek.TUESDAY, "Go to the university"));

        Family family1 = new Family(billFather, billMother);
        family1.addChild(Bill);

        family1.addPet(fish);
        family1.addPet(billPet);

        RoboCat oliverPet = new RoboCat("jerry", 4, 80, Set.of("playing"));
        Woman oliverMother = new Woman("Emma", "Johnson", 516904443005L, 90);
        Man oliverFather = new Man("Michael", "Johnson", 16903442217L, 90);
        Man Oliver = new Man("Oliver", "Johnson", 114344446456L, 50,
                Map.of(DayOfWeek.SATURDAY, "Hang out with friends", DayOfWeek.SUNDAY, "Play football"));

        Family family2 = new Family(oliverFather, oliverMother);
        family2.addChild(Oliver);
        family2.addPet(oliverPet);

        defaultFamilyService.saveFamily(family1);
        defaultFamilyService.saveFamily(family2);


    }
    public void createNewFamily() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Creating a new family:");


        System.out.print("Enter mother's first name: ");
        String motherFirstName = scanner.nextLine();
        System.out.print("Enter mother's last name: ");
        String motherLastName = scanner.nextLine();

        String motherDay, motherMonth, motherYear;
        Date motherBirthDate;
        while (true) {
            System.out.print("Enter mother's day of birth: ");
            motherDay = scanner.nextLine();
            System.out.print("Enter mother's month of birth: ");
            motherMonth = scanner.nextLine();
            System.out.print("Enter mother's year of birth: ");
            motherYear = scanner.nextLine();
            motherBirthDate = ConsoleUtils.parseDate(motherDay, motherMonth, motherYear);
            if (motherBirthDate != null) {
                break;
            }
        }
        System.out.print("Enter mother's IQ: ");
        int motherIQ = ConsoleUtils.getInputNumberValue(scanner, "Enter mother's IQ: ");


        System.out.print("Enter father's first name: ");
        String fatherFirstName = scanner.nextLine();
        System.out.print("Enter father's last name: ");
        String fatherLastName = scanner.nextLine();

        String fatherDay, fatherMonth, fatherYear;
        Date fatherBirthDate;
        while (true) {
            System.out.print("Enter mother's day of birth: ");
            fatherDay = scanner.nextLine();
            System.out.print("Enter mother's month of birth: ");
            fatherMonth = scanner.nextLine();
            System.out.print("Enter mother's year of birth: ");
            fatherYear = scanner.nextLine();
            fatherBirthDate = ConsoleUtils.parseDate(fatherDay, fatherMonth, fatherYear);
            if (fatherBirthDate != null) {
                break;
            }
        }
        System.out.print("Enter father's IQ: ");
        int fatherIQ = ConsoleUtils.getInputNumberValue(scanner, "It's not a number");

        Woman mother = new Woman(motherFirstName, motherLastName, motherBirthDate.getTime(), motherIQ);
        Man father = new Man(fatherFirstName, fatherLastName, fatherBirthDate.getTime(), fatherIQ);

        Family newFamily = new Family(father, mother);
        defaultFamilyService.saveFamily(newFamily);

        System.out.println("New family created successfully!");
    }
    public void editFamily() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the index of the family to edit: ");
        int familyIndex = ConsoleUtils.getInputNumberValue(scanner, "Invalid index. Please enter a valid integer index.");

        Family familyToEdit = defaultFamilyService.getFamilyById(familyIndex);
        if (familyToEdit == null) {
            System.out.println("Family with the given index not found.");
            return;
        }

        String editMenu = """
            1. Народити дитину
            2. Усиновити дитину
            3. Повернутися до головного меню
            """;

        while (true) {
            System.out.println(editMenu);
            System.out.print("Your option is: ");

            try {
                int option = ConsoleUtils.getInputNumberValue(scanner, "It's not a number");
                switch (option) {
                    case 1:
                        bornChildInFamily(familyToEdit);
                        break;
                    case 2:
                        adoptChildInFamily(familyToEdit);
                        break;
                    case 3:
                        return;
                    default:
                        System.out.println("Invalid option. Please enter a valid integer option.");
                        break;
                }
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a valid integer option.");
            }
        }


    }
    private void bornChildInFamily(Family family) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the gender of the child (male or female): ");
        String gender = scanner.nextLine();
        if (!gender.trim().equalsIgnoreCase("male") && !gender.trim().equalsIgnoreCase("female")) {
            System.out.println("Invalid gender. The child's gender can be either 'male' or 'female'.");
            return;
        }

        System.out.print("Enter the name of the child: ");
        String name = scanner.nextLine();

        if (gender.trim().equalsIgnoreCase("male")) {
            family.addChild(new Man(name, family.getFather().getSurname(), System.currentTimeMillis(), 0));
        } else {
            family.addChild(new Woman(name, family.getFather().getSurname(), System.currentTimeMillis(), 0));
        }
        if (family.countFamily() >= 5) {
            throw new FamilyOverflowException("Family size exceeds the limit for child birth!");
        }

        System.out.println("Child born successfully!");
    }
    private void adoptChildInFamily(Family family) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the gender of the child (male or female): ");
        String gender = scanner.nextLine();
        if (!gender.trim().equalsIgnoreCase("male") && !gender.trim().equalsIgnoreCase("female")) {
            System.out.println("Invalid gender. The child's gender can be either 'male' or 'female'.");
            return;
        }
        System.out.print("Enter the child's full name: ");
        String fullName = scanner.nextLine();
        String adoptChildDay, adoptChildMonth, adoptChildYear;
        Date adoptChildBirthDate;
        while (true) {
            System.out.print("Enter child's day of birth: ");
            adoptChildDay = scanner.nextLine();
            System.out.print("Enter child's month of birth: ");
            adoptChildMonth = scanner.nextLine();
            System.out.print("Enter child's year of birth: ");
            adoptChildYear = scanner.nextLine();
            adoptChildBirthDate = ConsoleUtils.parseDate(adoptChildDay, adoptChildMonth, adoptChildYear);
            if (adoptChildBirthDate != null) {
                break;
            }
        }

        System.out.print("Enter IQ: ");

        int iq = ConsoleUtils.getInputNumberValue(scanner, "Invalid IQ. Please enter a valid integer IQ.");



        if (gender.trim().equalsIgnoreCase("male")) {
            family.addChild(new Man(fullName, family.getFather().getSurname(), adoptChildBirthDate.getTime(), (family.getFather().getIq() + family.getMother().getIq()) / 2));
        } else {
            family.addChild(new Woman(fullName, family.getFather().getSurname(), adoptChildBirthDate.getTime(), (family.getFather().getIq() + family.getMother().getIq()) / 2));
        }
        if (family.countFamily() >= 5) {
            throw new FamilyOverflowException("Family size exceeds the limit for child birth!");
        }

        System.out.println("Child adopted successfully!");

    }



    public void menu() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Greetings, to continue, choose the option from the list below: ");

        String menu = """
                1. Заповнити тестовими даними
                2. Відобразити весь список сімей
                3. Відобразити список сімей, де кількість людей більша за задану
                4. Відобразити список сімей, де кількість людей менша за задану
                5. Підрахувати кількість сімей, де кількість членів дорівнює
                6. Створити нову родину
                7. Видалити сім'ю за індексом сім'ї у загальному списку
                8. Редагувати сім'ю за індексом сім'ї у загальному списку
                9. Видалити всіх дітей старше віку
                10. Зберегти дані локально
                11. Завантажити раніше збережені дані
                Write "exit" to exit the application
                """;

        int option;

        while (true) {
            System.out.println(menu);
            System.out.print("Your option is: ");
            String input = scanner.nextLine();

            if (input.trim().equalsIgnoreCase("exit")) {
                System.out.println("Exiting the application...");
                break;
            }

            try {
                option = Integer.parseInt(input);

            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a valid integer option.");
                continue;
            }



            switch (option) {
                case 1:
                    doAll();
                    break;
                case 2:
                    defaultFamilyService.displayAllFamilies();
                    break;
                case 3:
                    System.out.print("Enter the minimum number of people in a family: ");
                    int minPeople = ConsoleUtils.getInputNumberValue(scanner,  "It's not a number");
                    for (Family family : defaultFamilyService.getFamiliesBiggerThan(minPeople)) {
                        System.out.println(family.prettyFormat());
                    }
                    break;
                case 4:
                    System.out.print("Enter the maximum number of people in a family: ");
                    int maxPeople = ConsoleUtils.getInputNumberValue(scanner,  "It's not a number");
                    for (Family family : defaultFamilyService.getFamiliesLessThan(maxPeople)) {
                        System.out.println(family.prettyFormat());
                    }
                    break;
                case 5:
                    System.out.print("Enter the number of people in a family to count: ");
                    int countPeople = ConsoleUtils.getInputNumberValue(scanner,  "It's not a number");
                    int countFamilies = defaultFamilyService.countFamiliesWithMemberNumber(countPeople);
                    System.out.println("Number of families with " + countPeople + " members: " + countFamilies);
                    break;
                case 6:
                    createNewFamily();
                    break;
                case 7:
                    System.out.print("Enter the index of the family to delete: ");
                    int deleteIndex = ConsoleUtils.getInputNumberValue(scanner,  "It's not a number");
                    defaultFamilyService.deleteFamilyByIndex(deleteIndex);
                    break;
                case 8:
                    editFamily();
                    break;
                case 9:
                    System.out.print("Enter the age to delete children older than: ");
                    long ageToDelete = ConsoleUtils.getInputNumberValue(scanner,  "It's not a number");
                    defaultFamilyService.deleteAllChildrenOlderThen(ageToDelete);
                    break;
                case 10:
                defaultFamilyService.saveData();
                break;
                case 11:
                    defaultFamilyService.loadData();
                    break;
                default:
                    System.out.println("This option doesn't exist");
                    break;

            }

        }

    }


}


