import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class HumanTest {
    @Test
    public void testToStringPositiveScenario() {
        String name = "John";
        String surname = "Doe";
        int year = 44;
        int iq = 120;
        String[][] schedule = {
                {"Monday", "Math"},
                {"Tuesday", "Science"},
                {"Wednesday", "History"}
        };

        Human human = new Human(name, surname, year, iq, schedule);

        String expected = "Human{name='John', surname='Doe', year=44, iq=120, schedule=[[Monday, Math], [Tuesday, Science], [Wednesday, History]]}";
        String actual = human.toString();

        assertEquals(expected, actual);
    }
    @Test
    public void testToStringNegativeScenario() {
        String name = "Jane";
        String surname = "Smith";
        int year = 55;
        int iq = 110;
        String[][] schedule = {
                {"Monday", "Math"},
                {"Tuesday", "Science"},
                {"Wednesday", "History"}
        };

        Human human = new Human(name, surname, year, iq, schedule);

        String unexpected = "Human{name='Jane', surname='Smith', year=55, iq=200, schedule=[[Monday, Math], [Tuesday, Science], [Wednesday, History]]}";
        String actual = human.toString();

        assertNotEquals(unexpected, actual);
    }

    @Test
    public void testEqualsTrue() {
        Human John1 = new Human("John", "Rockefeller", 4, 55,
                new String[][]{{DayOfWeek.MONDAY.name(), "Go ot school"}, {DayOfWeek.FRIDAY.name(), "Go to the university"}});
        Human John2 = new Human("John", "Rockefeller", 4, 55,
                new String[][]{{DayOfWeek.MONDAY.name(), "Go ot school"}, {DayOfWeek.FRIDAY.name(), "Go to the university"}});
        assertTrue(John1.equals(John2));
    }

    @Test
    public void testEqualsFalse() {
        Human John1 = new Human("John", "Rockefeller", 4, 55,
                new String[][]{{DayOfWeek.MONDAY.name(), "Go ot school"}, {DayOfWeek.FRIDAY.name(), "Go to the university"}});
        Human John2 = new Human("Lionel", "Messi", 36);
        assertFalse(John1.equals(John2));
    }
    @Test
    public void testHashCodePositiveScenario() {
        Human John1 = new Human("John", "Rockefeller", 4, 55,
                new String[][]{{DayOfWeek.MONDAY.name(), "Go ot school"}, {DayOfWeek.FRIDAY.name(), "Go to the university"}});
        Human John2 = new Human("John", "Rockefeller", 4, 55,
                new String[][]{{DayOfWeek.MONDAY.name(), "Go ot school"}, {DayOfWeek.FRIDAY.name(), "Go to the university"}});
        assertEquals(John1.hashCode(), John2.hashCode());
    }

    @Test
    public void testHashCodeNegativeScenario() {
        Human John1 = new Human("John", "Rockefeller", 4, 55,
                new String[][]{{DayOfWeek.MONDAY.name(), "Go ot school"}, {DayOfWeek.FRIDAY.name(), "Go to the university"}});
        Human John2 = new Human("Lionel", "Messi", 36);
        assertNotEquals(John1.hashCode(), John2.hashCode());
    }
}
