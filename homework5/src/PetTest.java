import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class PetTest {
    @Test
    public void testToStringPositiveScenario() {
        String nickname = "Tom";
        int age = 3;
        int trickLevel = 5;
        String[] habits = {"Sleeping", "Eating"};

        Pet pet = new Pet(Species.CAT, nickname, age, trickLevel, habits);

        String expected = "CAT{nickname='Tom', age=3, trickLevel=5, habits=[Sleeping, Eating], canFly=false, hasFur=true, numberOfLegs=4}";
        String actual = pet.toString();

        assertEquals(expected, actual);
    }

    @Test
    public void testToStringNegativeScenario() {
        String nickname = "Max";
        int age = 2;
        int trickLevel = 3;
        String[] habits = {"Running", "Jumping"};

        Pet pet = new Pet(Species.DOG, nickname, age, trickLevel, habits);

        String unexpected = "DOG{nickname='Max', age=2, trickLevel=5, habits=[Running, Jumping], canFly=false, hasFur=true, numberOfLegs=4}";
        String actual = pet.toString();

        assertNotEquals(unexpected, actual);
    }

    @Test
    public void testEqualsTrue() {
        Pet pet1 = new Pet(Species.LION, "mufasa", 10, 90, new String[]{"hanging around"});
        Pet pet2 = new Pet(Species.LION, "mufasa", 10, 90, new String[]{"hanging around"});
        assertTrue(pet2.equals(pet1));
    }

    @Test
    public void testEqualsFalse() {
        Pet pet1 = new Pet(Species.LION, "mufasa", 10, 90, new String[]{"hanging around"});
        Pet pet2 = new Pet(Species.BIRD, "crow");
        assertFalse(pet2.equals(pet1));
    }

    @Test
    public void testHashCodePositiveScenario() {
        Pet pet1 = new Pet(Species.LION, "mufasa", 10, 90, new String[]{"hanging around"});
        Pet pet2 = new Pet(Species.LION, "mufasa", 10, 90, new String[]{"hanging around"});
        assertEquals(pet1.hashCode(), pet2.hashCode());
    }

    @Test
    public void testHashCodeNegativeScenario() {
        Pet pet1 = new Pet(Species.LION, "mufasa", 10, 90, new String[]{"hanging around"});
        Pet pet2 = new Pet(Species.BIRD, "crow");
        assertNotEquals(pet1.hashCode(), pet2.hashCode());
    }

}
