public class Main {
    public static void main(String[] args) {
        Human human1 = new Human();
        Pet pet = new Pet();

        Pet fish = new Pet(Species.FISH, "bob");

        Pet billPet = new Pet(Species.CAT, "tom", 5, 20, new String[]{"sleeping"});
        Human billMother = new Human("Sofia", "Gates", 45);
        Human billFather = new Human("Mark", "Gates", 55);
        Human Bill = new Human("Bill", "Gates", 66, 130,
                new String[][]{{DayOfWeek.MONDAY.name(), "Go ot school"}, {DayOfWeek.TUESDAY.name(), "Go to the university"}});

        Family family1 = new Family(billMother, billFather);
        family1.addChild(Bill);
        System.out.println(family1.toString());
        Bill.setPet(billPet);

        Pet oliverPet = new Pet(Species.MOUSE, "jerry", 4, 80, new String[]{"playing"});
        Human oliverMother = new Human("Emma", "Johnson", 56);
        Human oliverFather = new Human("Michael", "Johnson", 65);
        Human Oliver = new Human("Oliver", "Johnson", 22, 50,
                new String[][]{{DayOfWeek.SATURDAY.name(), "Hang out with friends"}, {DayOfWeek.SUNDAY.name(), "Play football"}});

        Family family2 = new Family(oliverMother, oliverFather);
        family2.addChild(Oliver);
        System.out.println(family2.toString());
        Oliver.setPet(oliverPet);

        System.out.println(Bill.toString());
        Bill.greetPet();
        Bill.describePet();

        System.out.println(Oliver.toString());
        Oliver.greetPet();
        Oliver.describePet();


        Bill.feedPet(billPet.getTimeToEat());

        System.out.println(family1.countFamily());
        family1.deleteChild(Bill);
        System.out.println(family1.countFamily());

        fish.respond();
        fish.foul();
        fish.eat();

        System.out.println(Bill.equals(Oliver));
        System.out.println(billPet.equals(oliverPet));

        System.out.println(fish.toString());



//       for (int i = 0; i < 10000000; i++) {
//            Human human = new Human();
//       }
    }

}
