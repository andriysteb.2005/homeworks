import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class FamilyTest {
    private Human mother;
    private Human father;
    private Family family;

    @BeforeEach
    public void setFamily() {
         mother = new Human("Jane", "Smith", 30);
         father = new Human("Will", "Smith", 30);
         family = new Family(mother, father);
    }

    @Test
    public void testAddChildPositiveScenario() {

        Human child = new Human("Child", "Smith", 34);

        family.addChild(child);

        Human[] expectedChildren = {child};
        Human[] actualChildren = family.getChildren();

        assertArrayEquals(expectedChildren, actualChildren);
        assertEquals(family, child.getFamily());
    }
    @Test
    public void testAddChildNegativeScenario() {
        Human child = new Human("Emma", "Smith", 15);

        family.addChild(child);

        Human[] expectedChildren = {child};
        Human[] actualChildren = family.getChildren();

        assertArrayEquals(expectedChildren, actualChildren);
        assertEquals(family, child.getFamily());
    }


    @Test
    public void testDeleteChildPositiveScenario() {

        Human child1 = new Human("Child1", "Smith", 30);
        Human child2 = new Human("Child2", "Smith", 30);

        family.addChild(child1);
        family.addChild(child2);

        boolean isChildDeleted = family.deleteChild(child1);

        assertTrue(isChildDeleted);
        assertNull(child1.getFamily());

        Human[] expectedChildren = {child2};
        Human[] actualChildren = family.getChildren();

        assertArrayEquals(expectedChildren, actualChildren);
    }


    @Test
    public void testDeleteChildNegativeScenario1() {
        Human child1 = new Human("Child1", "Smith", 20);
        Human child2 = new Human("Child2", "Smith", 21);
        Human child3 = new Human("Child3", "Smith", 25);

        family.addChild(child1);
        family.addChild(child2);

        boolean isChildDeleted = family.deleteChild(child3);

        assertFalse(isChildDeleted);

        Human[] expectedChildren = {child1, child2};
        Human[] actualChildren = family.getChildren();

        assertArrayEquals(expectedChildren, actualChildren);
    }
@Test
public void testDeleteChildByIndexPositiveScenario() {
    Human child1 = new Human("Child1", "Smith", 30);
    Human child2 = new Human("Child2", "Smith", 30);
    family.addChild(child1);
    family.addChild(child2);

    boolean result1 = family.deleteChild(0);
    assertTrue(result1);
    assertEquals(1, family.getChildren().length);
    assertEquals(child2, family.getChildren()[0]);

}
    @Test
    public void testDeleteChildByIndexNegativeScenario() {
        Human child1 = new Human("Child1", "Smith", 30);
        Human child2 = new Human("Child2", "Smith", 30);
        family.addChild(child1);
        family.addChild(child2);

        boolean result2 = family.deleteChild(1);
        assertTrue(result2);
        assertEquals(1, family.getChildren().length);
    }


    @Test
    public void testCountFamilyPositiveScenario() {

        Human child1 = new Human("Child1", "Smith", 3);
        Human child2 = new Human("Child2", "Smith", 4);

        family.addChild(child1);
        family.addChild(child2);

        int expectedFamilyCount = 4;
        int actualFamilyCount = family.countFamily();

        assertEquals(expectedFamilyCount, actualFamilyCount);
    }
    @Test
    public void testCountFamilyNegativeScenario() {

        Human child1 = new Human("Child1", "Smith", 10);
        Human child2 = new Human("Child2", "Smith", 11);

        family.addChild(child1);
        family.addChild(child2);

        int unexpectedFamilyCount = 3;
        int actualFamilyCount = family.countFamily();

        assertNotEquals(unexpectedFamilyCount, actualFamilyCount);
    }

    @Test
    public void testToStringPositiveScenario() {
        Human child = new Human("Child", "Smith", 5);
        Pet pet = new Pet(Species.DOG, "Buddy");

        Family family = new Family(mother, father);
        family.addChild(child);
        family.setPet(pet);

        String expected = "Family{mother=Human{name='Jane', surname='Smith', year=30, iq=0, schedule=null}, " +
                "father=Human{name='Will', surname='Smith', year=30, iq=0, schedule=null}, " +
                "children=[Human{name='Child', surname='Smith', year=5, iq=0, schedule=null}], " +
                "pet=DOG{nickname='Buddy', age=0, trickLevel=0, habits=null, canFly=false, hasFur=true, numberOfLegs=4}}";
        String actual = family.toString();

        assertEquals(expected, actual);
    }

    @Test
    public void testToStringNegativeScenario() {
        Human child = new Human("Child", "Smith", 6);
        Pet pet = new Pet(Species.DOG, "Buddy");

        family.addChild(child);
        family.setPet(pet);

        String unexpected = "Family{mother=Human{name='Jane', surname='Smith', year=25, iq=0, schedule=null}, " +
                "father=Human{name='Will', surname='Smith', year=31, iq=0, schedule=null}, " +
                "children=[Human{name='Child', surname='Smith', year=6, iq=0, schedule=null}], " +
                "pet=DOG{nickname='Buddy', age=0, trickLevel=0, habits=null, canFly=false, hasFur=true, numberOfLegs=4}}";
        String actual = family.toString();

        assertNotEquals(unexpected, actual);
    }


}

