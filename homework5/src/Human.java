import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Family family;
    private String[][] schedule;
    Random random = new Random();


    static {
        System.out.println("Class Human is loading...");
    }

    {
        System.out.println("The new object Human is creating\n");
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }


    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human() {
    }

    public boolean feedPet(boolean timeToEat) {
        int extraEating = random.nextInt(101);
        if (timeToEat) {
            System.out.println("I am feeding a pet");
            return true;
        } else if (pet.getTrickLevel() >= extraEating) {
            System.out.println("hmm... probably I will feed " + pet.getNickname());
            return true;
        } else {
            System.out.println("I think, " + pet.getNickname() + " is not hungry.");
            return false;
        }
    }

    public void greetPet() {
        System.out.println("Hello " + pet.getNickname());
    }

    public void describePet() {
        String trickLevelDescription = (pet.getTrickLevel() > 50) ? "very cunning" : "not very cunning";
        System.out.println("I have a " + pet.getSpecies() + ", it has " + pet.getAge() + " years old. It's " + trickLevelDescription);
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }


    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet ) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Human{name='" + name + "', surname='" + surname + "', year=" + year + ", iq=" + iq + ", schedule=" + Arrays.deepToString(schedule) + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Human human = (Human) o;
        if (human.hashCode() != o.hashCode()) {
            return false;
        }

        if (year != human.year || iq != human.iq) {
            return false;
        }
        if (!name.equals(human.name)) {
            return false;
        }
        return surname.equals(human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq);
    }
    protected void finalize(){
        System.out.println("Human is deleting!");
    }

}
enum DayOfWeek {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}

