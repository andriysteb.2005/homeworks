package Structure;

import Structure.controller.FamilyController;
import Structure.dao.CollectionFamilyDao;
import Structure.dao.FamilyDao;
import Structure.service.DefaultFamilyService;

public class Main {
    public static void main(String[] args) {
        FamilyDao familyDao = new CollectionFamilyDao();
        DefaultFamilyService defaultFamilyService = new DefaultFamilyService(familyDao);
        FamilyController familyController = new FamilyController(defaultFamilyService);
        familyController.menu();;
    }
}
