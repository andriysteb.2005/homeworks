package Structure.controller;

public class FamilyOverflowException extends RuntimeException {

    public FamilyOverflowException(String message) {
        super(message);
    }
}
