package Structure.model;

public enum Species {
    CAT(false, 4, true),
    ROBOCAT(false, 4, false),
    DOG(false, 4, true),
    BIRD(true, 2, false),
    HORSE(false, 4, true),
    FISH(false, 0, false),
    UNKNOWN(false,0, false);

    private   boolean canFly;
    private  int numberOfLegs;
    private  boolean hasFur;

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }


    public boolean getCanFly() {
        return canFly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public boolean getHasFur() {
        return hasFur;
    }

}
