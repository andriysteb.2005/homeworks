package Structure.model;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

public final class Woman extends Human{
    public void makeup(){
        System.out.println("I'm painting my lips now");
    }

    public Woman(String name, String surname, long birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    public Woman(String name, String surname, long birthDate, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public Woman() {
    }
    @Override
    public void greetPet() {
        if (getFamily() != null) {
            Set<Pet> pets = getFamily().getPets();
            for (Pet pet : pets) {
                System.out.println("Good boy " + pet.getNickname());
            }
        }
    }
    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String formattedBirthDate = dateFormat.format(new Date(getBirthDate()));
        return "Woman{name='" + getName() + "', surname='" + getSurname() + "', birthDate=" + formattedBirthDate + ", iq=" + getIq() + ", schedule=" + getSchedule() + "}";
    }
}
