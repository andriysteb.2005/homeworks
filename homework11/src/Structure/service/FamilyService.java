package Structure.service;

import Structure.model.Family;
import Structure.model.Human;
import Structure.model.Pet;

import java.util.List;

public interface FamilyService {

    List<Family> getAllFamilies();

    void displayAllFamilies();

    List<Family> getFamiliesBiggerThan(int number);

    List<Family> getFamiliesLessThan(int number);

    int countFamiliesWithMemberNumber(int number);

    void createNewFamily(Human father, Human mother);

    void deleteFamilyByIndex(int index);

    Family bornChild(Family family, String maleName, String femaleName);

    Family adoptChild(Family family, Human child);

    void deleteAllChildrenOlderThen(long birthDate);

    int count();

    Family getFamilyById(int index);

    List<Pet> getPets(int index);

    void addPet(int index, Pet pet);

    String prettyFormat(Family family);
}
