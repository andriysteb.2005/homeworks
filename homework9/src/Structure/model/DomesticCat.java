package Structure.model;

import java.util.Set;

public class DomesticCat extends Pet implements Foul {
    private final Species species = Species.CAT;

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void foul(){
        System.out.println("I need to cover my tracks well...");
    }

    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    public DomesticCat() {
    }

    @Override
    public void respond() {
        System.out.println("Hello owner, " + getNickname() + " missed you!");

    }
    @Override
    public String toString() {
        return this.species + "{nickname='" + getNickname() + "', age=" + getAge() +
                ", trickLevel=" + getTrickLevel() + ", habits=" + getHabits() +
                ", canFly=" + getSpecies().getCanFly() + ", hasFur=" + getSpecies().getHasFur() +
                ", numberOfLegs=" + getSpecies().getNumberOfLegs() + "}";
    }
}
