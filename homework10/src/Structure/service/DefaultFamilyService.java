package Structure.service;

import Structure.dao.FamilyDao;
import Structure.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class DefaultFamilyService implements FamilyService {

    private  FamilyDao familyDao;

    public DefaultFamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> families = getAllFamilies();
        families.stream()
                .forEach(family -> {
                    System.out.println("Family " + ":");
                    System.out.println(family);
                });
    }


    public List<Family> getFamiliesBiggerThan(int number) {
        return getAllFamilies().stream()
                .filter(family -> family.countFamily() > number)
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int number) {
        return getAllFamilies().stream()
                .filter(family -> family.countFamily() < number)
                .collect(Collectors.toList());
    }

    public int countFamiliesWithMemberNumber(int number) {
        return (int) getAllFamilies().stream()
                .filter(family -> family.countFamily() == number)
                .count();
    }

    public void createNewFamily(Human father, Human mother) {
        Family family = new Family((Man) father, (Woman) mother);
        familyDao.saveFamily(family);
    }

    public void deleteFamilyByIndex(int index) {
        List<Family> allFamilies = getAllFamilies();
        if (index >= 0 && index < allFamilies.size()) {
            familyDao.deleteFamily(allFamilies.get(index));
        }
    }

    public Family bornChild(Family family, String maleName, String femaleName) {
        Human child = family.bornChild();
        if (child instanceof Man) {
            child.setName(maleName);
        } else if (child instanceof Woman) {
            child.setName(femaleName);
        }
        familyDao.updateFamily(family);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        familyDao.updateFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(long birthDate) {
        List<Family> allFamilies = getAllFamilies();
        allFamilies.forEach(family -> {
            List<Human> childrenToRemove = family.getChildren().stream()
                    .filter(child -> child.getBirthDate() < birthDate)
                    .collect(Collectors.toList());
            family.removeChildren(childrenToRemove);
            familyDao.updateFamily(family);
        });
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public  List<Pet> getPets(int index) {
        Family family = familyDao.getFamilyByIndex(index);
        if (family != null) {
            return family.getPets().stream().toList();
        }
        return new ArrayList<>();
    }

    public void addPet(int index, Pet pet) {
        Family family = familyDao.getFamilyByIndex(index);
        if (family != null) {
            family.addPet(pet);
            familyDao.updateFamily(family);
        }
    }
    public void saveFamily(Family family){
        familyDao.saveFamily(family);
    }


}
