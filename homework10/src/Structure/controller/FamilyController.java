package Structure.controller;

import Structure.model.*;
import Structure.service.DefaultFamilyService;

import java.util.Map;
import java.util.Set;

public class FamilyController  {
    private static DefaultFamilyService defaultFamilyService;


    public FamilyController(DefaultFamilyService defaultFamilyService) {
        this.defaultFamilyService = defaultFamilyService;
    }

    public static void doAll() {

        Human human1 = new Human();


        Fish fish = new Fish("bob");

        DomesticCat billPet = new DomesticCat("tom", 5, 20, Set.of("sleeping"));
        Woman billMother = new Woman("Sofia", "Gates", 610945400005L, 120);
        Man billFather = new Man("Mark", "Gates", 51690330005L, 100);
        Man Bill = new Man("Bill", "Gates", 216055L, 130,
                Map.of(DayOfWeek.MONDAY, "Go ot school", DayOfWeek.TUESDAY, "Go to the university"));

        Family family1 = new Family(billFather, billMother);
        family1.addChild(Bill);
        System.out.println(family1.toString());

        family1.addPet(fish);
        family1.addPet(billPet);

        RoboCat oliverPet = new RoboCat("jerry", 4, 80, Set.of("playing"));
        Woman oliverMother = new Woman("Emma", "Johnson", 516904443005L, 90);
        Man oliverFather = new Man("Michael", "Johnson", 16903442217L, 90);
        Man Oliver = new Man("Oliver", "Johnson", 114344446456L, 50,
                Map.of(DayOfWeek.SATURDAY, "Hang out with friends", DayOfWeek.SUNDAY, "Play football"));

        Family family2 = new Family(oliverFather, oliverMother);
        family2.addChild(Oliver);
        System.out.println(family2.toString());
        family2.addPet(oliverPet);

        System.out.println(Bill.toString());
        Bill.greetPet();
        Bill.describePet();

        System.out.println(Oliver.toString());
        Oliver.greetPet();
        Oliver.describePet();


        Bill.feedPet(billPet.getTimeToEat());

        System.out.println(family1.countFamily());
        System.out.println(family1.countFamily());


        fish.respond();
        fish.foul();
        fish.eat();

        System.out.println(Bill.equals(Oliver));
        System.out.println(billPet.equals(oliverPet));

        System.out.println(fish.toString());


        System.out.println(family1.countFamily());

        System.out.println(family1.bornChild().toString());
        System.out.println(family1.countFamily());
        System.out.println(family1.toString());

        family1.setChild(family1.bornChild());
        Bill.describePet();


        defaultFamilyService.saveFamily(family1);
        defaultFamilyService.saveFamily(family2);
        defaultFamilyService.addPet(1, oliverPet);
        defaultFamilyService.displayAllFamilies();
        defaultFamilyService.createNewFamily(oliverFather, billMother);
        defaultFamilyService.deleteFamilyByIndex(5);
        defaultFamilyService.bornChild(family1, "bob", "anna");
        defaultFamilyService.adoptChild(family1, Oliver);
        defaultFamilyService.deleteAllChildrenOlderThen(-1);
        System.out.println(defaultFamilyService.getPets(0));
        defaultFamilyService.displayAllFamilies();

        System.out.println(Bill.describeAge());
        System.out.println(Oliver.describeAge());
        System.out.println(Bill.toString());



    }

}


