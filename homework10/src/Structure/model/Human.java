package Structure.model;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Family family;
    private Map<DayOfWeek, String> schedule;
    Random random = new Random();


    static {
        System.out.println("Class Human is loading...");
    }

    {
        System.out.println("The new object Human is creating\n");
    }

    public Human(String name, String surname, long birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
    }


    public Human(String name, String surname, long birthDate, int iq, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human() {
    }
    public Human(String name, String surname, String birthDateStr, int iq) {
        this.name = name;
        this.surname = surname;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate birthDate = LocalDate.parse(birthDateStr, formatter);
        LocalDateTime startOfDay = birthDate.atStartOfDay();
        this.birthDate = startOfDay.toInstant(ZoneOffset.UTC).toEpochMilli();
        this.iq = iq;
    }
    public String describeAge() {
        LocalDate currentDate = LocalDate.now();
        Instant instant = Instant.ofEpochMilli(birthDate);
        LocalDate birthDateTime = LocalDate.ofInstant(instant, ZoneId.systemDefault());

        long years = birthDateTime.until(currentDate, ChronoUnit.YEARS);
        birthDateTime = birthDateTime.plusYears(years);

        long months = birthDateTime.until(currentDate, ChronoUnit.MONTHS);
        birthDateTime = birthDateTime.plusMonths(months);

        long days = birthDateTime.until(currentDate, ChronoUnit.DAYS);

        return String.format("%d years, %d months, and %d days", years, months, days);
    }

    public boolean feedPet(boolean timeToEat) {
        if (family != null) {
            Set<Pet> pets = family.getPets();
            for (Pet pet : pets) {

                int extraEating = random.nextInt(101);
                if (timeToEat) {
                    System.out.println("I am feeding a pet");
                    return true;
                } else if (pet.getTrickLevel() >= extraEating) {
                    System.out.println("hmm... probably I will feed " + pet.getNickname());
                    return true;
                } else {
                    System.out.println("I think, " + pet.getNickname() + " is not hungry.");
                    return false;
                }
            }
        }
        return false;
    }

    public void greetPet() {
        if (family != null) {
            Set<Pet> pets = family.getPets();
            for (Pet pet : pets) {
                System.out.println("Hello " + pet.getNickname());
            }
        }
    }

    public void describePet() {
        if (family != null) {
            Set<Pet> pets = family.getPets();
            for (Pet pet : pets) {
                String trickLevelDescription = (pet.getTrickLevel() > 50) ? "very cunning" : "not very cunning";
                System.out.println("I have a " + pet.getSpecies() + ", it has " + pet.getAge() + " years old. It's " + trickLevelDescription);
            }
        }
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }


    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    public Set<Pet> getPet() {
        return family.getPets();
    }

    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String formattedBirthDate = dateFormat.format(new Date(birthDate));
        return "Human{name='" + name + "', surname='" + surname + "', birthDate=" + formattedBirthDate + ", iq=" + iq + ", schedule=" + schedule + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Human human = (Human) o;
        if (human.hashCode() != o.hashCode()) {
            return false;
        }

        if (birthDate != human.birthDate || iq != human.iq) {
            return false;
        }
        if (!name.equals(human.name)) {
            return false;
        }
        return surname.equals(human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq);
    }


}



