package Structure.dao;

import Structure.model.Family;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> familyList;

    public CollectionFamilyDao() {
        familyList = new ArrayList<>();
    }

    @Override
    public List<Family> getAllFamilies() {
        return familyList;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index < familyList.size()) {
            return familyList.get(index);
        }
        return null;
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= 0 && index < familyList.size()) {
            familyList.remove(index);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        return familyList.remove(family);
    }

    @Override
    public  void saveFamily(Family family) {
        if (familyList.contains(family)) {
            int index = familyList.indexOf(family);
            familyList.set(index, family);
        } else {
            familyList.add(family);
        }
    }

    @Override
    public void updateFamily(Family family) {
        int index = familyList.indexOf(family);
        if (index != -1) {
            familyList.set(index, family);
        }
    }
}
