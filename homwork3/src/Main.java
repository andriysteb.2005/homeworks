import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String[][] schedule = new String[7][2];
        schedule[0][0] = "Monday";
        schedule[0][1] = "Go to university";
        schedule[1][0] = "Tuesday";
        schedule[1][1] = "Visit courses";
        schedule[2][0] = "Wednesday";
        schedule[2][1] = "Make homework";
        schedule[3][0] = "Thursday";
        schedule[3][1] = "Go to the gym";
        schedule[4][0] = "Friday";
        schedule[4][1] = "No plans";
        schedule[5][0] = "Saturday";
        schedule[5][1] = "Meet with friends";
        schedule[6][0] = "Sunday";
        schedule[6][1] = "Visit grandparents";

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Please, input the day of the week (or type 'exit' to quit):");
            String userInput = scanner.nextLine().trim().toLowerCase();

            if (userInput.equalsIgnoreCase("exit")) {
                System.out.println("Exiting the program...");
                break;
            } else if (userInput.startsWith("change") || userInput.startsWith("reschedule")) {
                String[] inputParts = userInput.split(" ", 2);
                if (inputParts.length != 2) {
                    System.out.println("Invalid command. Please try again.");
                    continue;
                }

                String dayOfWeek = inputParts[1].trim().toLowerCase();

                switch (dayOfWeek) {
                    case "monday":
                        System.out.println("Please, input new tasks for Monday:");
                        String newTasksMonday = scanner.nextLine();
                        schedule[0][1] = newTasksMonday;
                        System.out.println("Tasks for Monday have been updated.");
                        break;
                    case "tuesday":
                        System.out.println("Please, input new tasks for Tuesday:");
                        String newTasksTuesday = scanner.nextLine();
                        schedule[1][1] = newTasksTuesday;
                        System.out.println("Tasks for Tuesday have been updated.");
                        break;
                    case "wednesday":
                        System.out.println("Please, input new tasks for Wednesday:");
                        String newTasksWednesday = scanner.nextLine();
                        schedule[2][1] = newTasksWednesday;
                        System.out.println("Tasks for Wednesday have been updated.");
                        break;
                    case "thursday":
                        System.out.println("Please, input new tasks for Thursday:");
                        String newTasksThursday = scanner.nextLine();
                        schedule[3][1] = newTasksThursday;
                        System.out.println("Tasks for Thursday have been updated.");
                        break;
                    case "friday":
                        System.out.println("Please, input new tasks for Friday:");
                        String newTasksFriday = scanner.nextLine();
                        schedule[4][1] = newTasksFriday;
                        System.out.println("Tasks for Friday have been updated.");
                        break;
                    case "saturday":
                        System.out.println("Please, input new tasks for Saturday:");
                        String newTasksSaturday = scanner.nextLine();
                        schedule[5][1] = newTasksSaturday;
                        System.out.println("Tasks for Saturday have been updated.");
                        break;
                    case "sunday":
                        System.out.println("Please, input new tasks for Sunday:");
                        String newTasksSunday = scanner.nextLine();
                        schedule[6][1] = newTasksSunday;
                        System.out.println("Tasks for Sunday have been updated.");
                        break;
                    default:
                        System.out.println("Invalid day of the week. Please try again.");
                        break;
                }
            } else {
                switch (userInput) {
                    case "monday":
                        System.out.println("Your tasks for " + schedule[0][0] + ": " + schedule[0][1]);
                        break;
                    case "tuesday":
                        System.out.println("Your tasks for " + schedule[1][0] + ": " + schedule[1][1]);
                        break;
                    case "wednesday":
                        System.out.println("Your tasks for " + schedule[2][0] + ": " + schedule[2][1]);
                        break;
                    case "thursday":
                        System.out.println("Your tasks for " + schedule[3][0] + ": " + schedule[3][1]);
                        break;
                    case "friday":
                        System.out.println("Your tasks for " + schedule[4][0] + ": " + schedule[4][1]);
                        break;
                    case "saturday":
                        System.out.println("Your tasks for " + schedule[5][0] + ": " + schedule[5][1]);
                        break;
                    case "sunday":
                        System.out.println("Your tasks for " + schedule[6][0] + ": " + schedule[6][1]);
                        break;
                    default:
                        System.out.println("Sorry, I don't understand you, please try again.");
                        break;
                }
            }
        }
    }
}
