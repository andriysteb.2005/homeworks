
import java.util.Map;
import java.util.Set;

public final class Man extends Human{
    public void repairCar(){
        System.out.println("Give me the key");
    }

    public Man(String name, String surname, int year, int iq) {
        super(name, surname, year, iq);
    }

    public Man(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man() {
    }


    @Override
    public void greetPet() {
        if (getFamily() != null) {
            Set<Pet> pets = getFamily().getPets();
            for (Pet pet : pets) {
                System.out.println("Good boy " + pet.getNickname());
            }
        }
    }
    @Override
    public String toString() {
        return "Man{name='" + getName() + "', surname='" + getSurname() + "', year=" + getYear() + ", iq=" + getIq() + ", schedule=" + getSchedule() + "}";
    }
}
