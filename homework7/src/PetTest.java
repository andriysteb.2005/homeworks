import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PetTest {
    @Test
    public void testToString() {
        Pet pet = new DomesticCat("Tom", 3, 5, Set.of("Sleeping", "Eating"));
        String expected = "CAT{nickname='Tom', age=3, trickLevel=5, habits=[Sleeping, Eating], canFly=false, hasFur=true, numberOfLegs=4}";
        String actual = pet.toString();
        assertEquals(expected, actual);
    }

    @Test
    public void testEqualsTrue() {
        Pet pet1 = new DomesticCat("mufasa", 10, 90, Set.of("hanging around"));
        Pet pet2 = new DomesticCat("mufasa", 10, 90, Set.of("hanging around"));
        assertTrue(pet2.equals(pet1));
    }

    @Test
    public void testEqualsFalse() {
        Pet pet1 = new Dog("mufasa", 10, 90, Set.of("hanging around"));
        Pet pet2 = new RoboCat("robo");
        assertFalse(pet2.equals(pet1));
    }

    @Test
    public void testHashCode() {
        Pet pet1 = new DomesticCat("mufasa", 10, 90, Set.of("hanging around"));
        Pet pet2 = new DomesticCat("mufasa", 10, 90, Set.of("hanging around"));
        assertEquals(pet1.hashCode(), pet2.hashCode());
    }
}
