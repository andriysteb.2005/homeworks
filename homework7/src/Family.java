import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Random;
import java.util.Objects;


public class Family implements HumanCreator {
    private Man father;
    private Woman mother;
    private Random random;
    private Human child;
    private String[] maleNames = new String[]{"John", "Michael", "William", "David", "James"};
    private String[] femaleNames = new String[]{"Emily", "Emma", "Olivia", "Sophia", "Ava"};
    private List<Human> children;
    private Set<Pet> pets;

    static {
        System.out.println("Class Family is loading...");
    }

    {
        random = new Random();
        System.out.println("The new object Family is creating\n");
    }

    public Family(Man father, Woman mother) {
        this.father = father;
        this.mother = mother;
        this.children = new ArrayList<>();
        this.pets = new HashSet<>();
    }

    @Override
    public Human bornChild() {
        boolean isMale = random.nextBoolean();
        String firstName = getRandomName(isMale);
        int iq = (father.getIq() + mother.getIq()) / 2;
        if (isMale) {
            child = new Man(firstName, father.getSurname(), 0, iq);
        } else {
            child = new Woman(firstName, father.getSurname(), 0, iq);
        }
        addChild(child);
        return child;
    }

    private String getRandomName(boolean isMale) {
        if (isMale) {
            int index = random.nextInt(maleNames.length);
            return maleNames[index];
        } else {
            int index = random.nextInt(femaleNames.length);
            return femaleNames[index];
        }
    }

    public void addChild(Human child) {
        child.setFamily(this);
        children.add(child);
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index >= children.size()) {
            return false;
        }
        Human child = children.remove(index);
        child.setFamily(null);
        return true;
    }

    public boolean deleteChild(Human child) {
        return children.remove(child);
    }

    public int countFamily() {
        return children.size() + 2;
    }

    public Human getChild() {
        return child;
    }

    public void setChild(Human child) {
        this.child = child;
    }

    public Woman getMother() {
        return mother;
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }

    public Man getFather() {
        return father;
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    public void addPet(Pet pet) {
        pets.add(pet);
    }

    public void removePet(Pet pet) {
        pets.remove(pet);
    }



    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pets=" + pets +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Objects.equals(children, family.children) &&
                Objects.equals(pets, family.pets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pets);
    }

    @Override
    protected void finalize() {
        System.out.println("Family is deleting!");
    }
}
