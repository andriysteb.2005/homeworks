import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HumanTest {
    @Test
    public void testToString() {
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "Work");
        schedule.put(DayOfWeek.TUESDAY, "Gym");

        Human human = new Human("John", "Doe", 50, 120, schedule);
        String expected = "Human{name='John', surname='Doe', year=1990, iq=120, schedule=" + schedule + "}";
        String actual = human.toString();
        assertEquals(expected, actual);
    }

    @Test
    public void testEqualsTrue() {
        Human human1 = new Human("John", "Doe", 50, 120);
        Human human2 = new Human("John", "Doe", 50, 120);
        assertTrue(human2.equals(human1));
    }

    @Test
    public void testEqualsFalse() {
        Human human1 = new Human("John", "Doe", 50, 120);
        Human human2 = new Human("Jane", "Doe", 50, 120);
        assertFalse(human2.equals(human1));
    }

    @Test
    public void testHashCode() {
        Human human1 = new Human("John", "Doe", 50, 120);
        Human human2 = new Human("John", "Doe", 50, 120);
        assertEquals(human1.hashCode(), human2.hashCode());
    }
}
