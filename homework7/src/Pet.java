
import java.util.Set;
import java.util.Random;
import java.util.Objects;

public abstract class Pet {
    private final Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits;

    private boolean timeToEat;
    Random random = new Random();

    static {
        System.out.println("Class Pet is loading...");
    }

    {
        System.out.println("The new object Pet is creating\n");
    }

    public boolean getTimeToEat() {
        int number = random.nextInt(2);
        return number == 0;
    }

    public Pet( String nickname) {
        this.nickname = nickname;
    }

    public Pet( String nickname, int age, int trickLevel, Set<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {
    }

    public void eat() {
        System.out.println("I'm eating!");
    }

    public abstract void respond();


    public Species getSpecies() {
        return species;
    }
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public  Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    public boolean isTimeToEat() {
        return timeToEat;
    }

    public void setTimeToEat(boolean timeToEat) {
        this.timeToEat = timeToEat;
    }

    @Override
    public String toString() {
        return species + "{nickname='" + nickname + "', age=" + age +
                ", trickLevel=" + trickLevel + ", habits=" + habits +
                ", canFly=" + species.getCanFly() + ", hasFur=" + species.getHasFur() +
                ", numberOfLegs=" + species.getNumberOfLegs() + "}";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pet pet = (Pet) o;
        if (pet.hashCode() != o.hashCode()) {
            return false;
        }
        if (age != pet.age || trickLevel != pet.trickLevel) {
            return false;
        }
        if (!nickname.equals(pet.nickname)) {
            return false;
        }
        return species.equals(pet.species);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel);
    }
    protected void finalize(){
        System.out.println("Pet is deleting!");
    }
}
enum Species {
    CAT(false, 4, true),
    ROBOCAT(false, 4, false),
    DOG(false, 4, true),
    BIRD(true, 2, false),
    HORSE(false, 4, true),
    FISH(false, 0, false),
    UNKNOWN(false,0, false);

    private   boolean canFly;
    private  int numberOfLegs;
    private  boolean hasFur;

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }


    public boolean getCanFly() {
        return canFly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public boolean getHasFur() {
        return hasFur;
    }
}
