import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FamilyTest {
    @Test
    public void testBornChild() {
        Man father = new Man("John", "Smith", 35, 120);
        Woman mother = new Woman("Jane", "Smith", 32, 110);

        Family family = new Family(father, mother);

        Human child = family.bornChild();

        assertNotNull(child);
        assertTrue(child instanceof Human);
        assertEquals(family, child.getFamily());
        assertTrue(family.getChildren().contains(child));
    }

    @Test
    public void testDeleteChildByIndex() {
        Man father = new Man("John", "Smith", 35, 120);
        Woman mother = new Woman("Jane", "Smith", 32, 110);

        Family family = new Family(father, mother);

        Human child1 = family.bornChild();
        Human child2 = family.bornChild();
        Human child3 = family.bornChild();

        int initialSize = family.getChildren().size();

        boolean result = family.deleteChild(1);

        assertTrue(result);
        assertEquals(initialSize - 1, family.getChildren().size());
        assertFalse(family.getChildren().contains(child2));
    }

    @Test
    public void testDeleteChild() {
        Man father = new Man("John", "Smith", 35, 120);
        Woman mother = new Woman("Jane", "Smith", 32, 110);

        Family family = new Family(father, mother);

        Human child1 = family.bornChild();
        Human child2 = family.bornChild();
        Human child3 = family.bornChild();

        int initialSize = family.getChildren().size();

        boolean result = family.deleteChild(child2);

        assertTrue(result);
        assertEquals(initialSize - 1, family.getChildren().size());
        assertFalse(family.getChildren().contains(child2));
    }

    @Test
    public void testCountFamily() {
        Man father = new Man("John", "Smith", 35, 120);
        Woman mother = new Woman("Jane", "Smith", 32, 110);

        Family family = new Family(father, mother);

        int expectedFamilyCount = family.getChildren().size() + 2;

        assertEquals(expectedFamilyCount, family.countFamily());
    }

    @Test
    public void testAddPet() {
        Man father = new Man("John", "Smith", 35, 120);
        Woman mother = new Woman("Jane", "Smith", 32, 110);

        Family family = new Family(father, mother);

        Pet pet = new Dog("Dog");

        family.addPet(pet);

        assertTrue(family.getPets().contains(pet));
    }

    @Test
    public void testRemovePet() {
        Man father = new Man("John", "Smith", 35, 120);
        Woman mother = new Woman("Jane", "Smith", 32, 110);

        Family family = new Family(father, mother);

        Pet pet1 = new Dog("Dog");
        Pet pet2 = new DomesticCat("Cat");
        Pet pet3 = new Fish("Fish");

        family.addPet(pet1);
        family.addPet(pet2);
        family.addPet(pet3);

        int initialSize = family.getPets().size();

        family.removePet(pet2);

        assertFalse(family.getPets().contains(pet2));
        assertEquals(initialSize - 1, family.getPets().size());
    }

    @Test
    public void testToString() {
        Man father = new Man("John", "Smith", 35, 120);
        Woman mother = new Woman("Jane", "Smith", 32, 110);

        Family family = new Family(father, mother);


        Pet pet1 = new Dog("Dog");
        Pet pet2 = new DomesticCat("Cat");

        family.addPet(pet1);
        family.addPet(pet2);

        String expectedString = "Family{mother=Woman{name='Jane', surname='Smith', year=32, iq=0, schedule=null}, father=Man{name='John', surname='Smith', year=35, iq=0, schedule=null}, children=[], pets=[DOG{nickname='Dog', age=0, trickLevel=0, habits=null, canFly=false, hasFur=true, numberOfLegs=4}, CAT{nickname='Cat', age=0, trickLevel=0, habits=null, canFly=false, hasFur=true, numberOfLegs=4}]}";
        String actual = family.toString();

        assertEquals(expectedString , actual);
    }
}
