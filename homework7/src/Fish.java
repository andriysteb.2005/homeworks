
import java.util.Set;

public class Fish extends Pet implements Foul {
    private final Species species = Species.FISH;

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void foul(){
        System.out.println("Bul bul bul...");
    }

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Fish() {
    }

    @Override
    public void respond() {
        System.out.println("Hello owner, " + getNickname() + " missed you!");
    }


    @Override
    public String toString() {
        return this.species + "{nickname='" + getNickname() + "', age=" + getAge() +
                ", trickLevel=" + getTrickLevel() + ", habits=" + getHabits() +
                ", canFly=" + getSpecies().getCanFly() + ", hasFur=" + getSpecies().getHasFur() +
                ", numberOfLegs=" + getSpecies().getNumberOfLegs() + "}";
    }
}
